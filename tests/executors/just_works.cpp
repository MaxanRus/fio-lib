#include <gtest/gtest.h>

#include <atomic>
#include <chrono>
#include <executors/compute/thread_pool.hpp>
#include <executors/execute.hpp>
#include <set>
#include <thread>

using namespace fio::executor;
using namespace fio::executor::compute;

TEST(Executors, JustWorks) {
  ThreadPool tp(4);
  tp.WaitIdle();
  tp.Stop();
}

TEST(Executors, SimpleTask) {
  ThreadPool tp(1);

  std::atomic<bool> flag{false};
  Execute(tp, [&flag]() { flag.store(true); });

  tp.WaitIdle();
  tp.Stop();

  ASSERT_TRUE(flag);
}

class DifferentThreadsTest : public testing::Test {
  using ThreadId = std::thread::id;
  static constexpr auto GetId = std::this_thread::get_id;

 public:
  DifferentThreadsTest() = default;

  void Test() {
    {
      std::lock_guard g(guard_thread_ids);
      thread_ids.emplace(GetId());

      if (thread_ids.size() != count_threads) {
        Execute(tp, &DifferentThreadsTest::Test, this);
      }
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(7));
  }

  void Stop() {
    tp.WaitIdle();
    tp.Stop();
  }

  void CheckCount() { ASSERT_EQ(count_threads, thread_ids.size()); }

 private:
  const size_t count_threads = 16;
  std::set<ThreadId> thread_ids;

  ThreadPool tp{count_threads};
  std::mutex guard_thread_ids;
};

TEST_F(DifferentThreadsTest, 16_threads) {
  Test();
  Stop();
  CheckCount();
}

class RunAllTasks : public testing::Test {
 public:
  RunAllTasks() {}

  void Bar() { count_tasks.fetch_add(1); }

  void Foo() {
    for (size_t i = 0; i < count_bar; ++i) {
      Execute(tp, &RunAllTasks::Bar, this);
    }
  }

  void Start() {
    for (size_t i = 0; i < count_foo; ++i) {
      Execute(tp, &RunAllTasks::Foo, this);
    }
  }

  void Stop() {
    tp.WaitIdle();
    tp.Stop();
  }

  void Check() { ASSERT_EQ(count_tasks.load(), count_foo * count_bar); }

 private:
  const size_t count_threads = 4;

  const size_t count_foo = 10;
  const size_t count_bar = 10;

  ThreadPool tp{count_threads};
  std::atomic<size_t> count_tasks{0};
};

TEST_F(RunAllTasks, CheckRunAllTasks) {
  Start();
  Stop();
  Check();
}

