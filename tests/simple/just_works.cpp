#include <gtest/gtest.h>

#include <coroutine/coroutine.hpp>

using namespace fio::coroutine;

namespace tests::coroutine {
int counter = 0;

void foo() { counter += 1; }

void bar(int x) {
  foo();
  Coroutine::Suspend();
  counter += x;
  Coroutine::Suspend();
  foo();
}
}  // namespace tests::coroutine

TEST(Simple, JustWorks) {
  Coroutine f(&tests::coroutine::foo);
  Coroutine s(&tests::coroutine::bar, 7);

  ASSERT_EQ(tests::coroutine::counter, 0);
  s.Resume();
  ASSERT_EQ(tests::coroutine::counter, 1);
  f.Resume();
  ASSERT_EQ(tests::coroutine::counter, 2);
  s.Resume();
  ASSERT_EQ(tests::coroutine::counter, 9);
  s.Resume();
  ASSERT_EQ(tests::coroutine::counter, 10);
}

