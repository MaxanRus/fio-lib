#include <gtest/gtest.h>

#include <fiber/api.hpp>
#include <fiber/io/socket.hpp>
#include <executors/compute/thread_pool.hpp>

namespace {
using namespace fio::fiber;

int port = 7777;

io::Socket CreateServer() {
  io::Socket s;
  s.Bind(port);
  s.Listen(10);

  return s;
}

void Server(io::Socket s) {
  std::optional<io::Socket> clients[10];
  for (size_t i = 0; i < 10; ++i) {
    clients[i].emplace(s.Accept());
  }

  std::array<char, 20> buffer;
  for (size_t i = 0; i < 10; ++i) {
    auto& client = clients[i].value();
    size_t size = client.Read({buffer.begin(), buffer.end()});

    std::string_view result{buffer.begin(), size};
    ASSERT_EQ(result, "hello world");
  }

  for (size_t i = 0; i < 10; ++i) {
    auto& client = clients[i].value();
    client.Write("hello client");
  }

  for (size_t i = 0; i < 10; ++i) {
    auto& client = clients[i].value();
    client.Close();
  }
  s.Close();
}

void Client() {
  io::Socket s;
  s.Connect("127.0.0.1", port);
  const char* str = "hello world";
  s.Write(str);

  std::array<char, 20> buffer;
  size_t size = s.Read({buffer.begin(), buffer.end()});

  std::string_view result{buffer.begin(), size};
  ASSERT_EQ(result, "hello client");

  s.Close();
}

TEST(fiber_socket, multi_connect) {
  fio::executor::compute::ThreadPool tp{1};
  Go(tp, Server, CreateServer());
  for (size_t i = 0; i < 10; ++i) {
    Go(tp, Client);
  }

  tp.WaitIdle();
  tp.Stop();
}
}  // namespace
