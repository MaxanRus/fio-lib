#include <gtest/gtest.h>

#include <fiber/api.hpp>
#include <fiber/io/socket.hpp>
#include <executors/compute/thread_pool.hpp>

namespace {
using namespace fio::fiber;

int port = 7777;

io::Socket CreateServer() {
  io::Socket s;
  s.Bind(port);
  s.Listen(10);

  return s;
}

void Server(io::Socket s) {
  auto client = s.Accept();

  std::array<char, 20> buffer;
  size_t size = client.Read({buffer.begin(), buffer.end()});

  std::string_view result{buffer.begin(), size};
  ASSERT_EQ(result, "hello world");

  client.Write("hello client");

  client.Close();
  s.Close();
}

void Client() {
  io::Socket s;
  s.Connect("127.0.0.1", port);
  const char* str = "hello world";
  s.Write(str);

  std::array<char, 20> buffer;
  size_t size = s.Read({buffer.begin(), buffer.end()});

  std::string_view result{buffer.begin(), size};
  ASSERT_EQ(result, "hello client");

  s.Close();
}

TEST(fiber_socket, simple) {
  fio::executor::compute::ThreadPool tp{1};

  Go(tp, Server, CreateServer());
  Go(tp, Client);

  tp.WaitIdle();
  tp.Stop();
}
}  // namespace
