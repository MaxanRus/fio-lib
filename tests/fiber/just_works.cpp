#include <gtest/gtest.h>

#include <executors/compute/thread_pool.hpp>
#include <fiber/api.hpp>

using namespace fio::fiber;

namespace tests::fiber {
int counter = 0;
void bar() { counter++; }
void foo() {
  if (counter == 0) {
    Go(&bar);
  }
  counter++;
  Yield();
  counter++;
}
}  // namespace tests::fiber

TEST(Fiber, JustWorks) {
  fio::executor::compute::ThreadPool tp(4);

  Go(tp, &tests::fiber::foo);

  tp.WaitIdle();
  tp.Stop();

  ASSERT_EQ(tests::fiber::counter, 3);
}

namespace test::fiber {

class TesterConcurrent {
 public:
  TesterConcurrent(size_t count_fibers)
      : count_iterations_(10 * count_fibers), count_fibers_(count_fibers) {}

  void Test() {
    fio::executor::compute::ThreadPool tp(1);

    for (size_t i = 0; i < count_fibers_; ++i) {
      Go(tp, &TesterConcurrent::Worker, this, count_fibers_ - i - 1);
    }

    tp.WaitIdle();
    tp.Stop();
  }

 private:
  void Worker(size_t index) {
    while (counter < count_iterations_) {
      if (counter % count_fibers_ != index) {
        Yield();
      } else {
        counter++;
      }
    }
  }

  size_t counter = 0;
  size_t count_iterations_;
  size_t count_fibers_;
};

}  // namespace test::fiber

TEST(Fiber, Concurent) {
  test::fiber::TesterConcurrent(2).Test();
  test::fiber::TesterConcurrent(5).Test();
  test::fiber::TesterConcurrent(10).Test();
  test::fiber::TesterConcurrent(20).Test();
}

