#include <executors/executor.hpp>
#include <functional>

namespace fio::executor {

namespace support {

template <typename... FArgs>
class SimpleTask : public ITask {
 public:
  SimpleTask(FArgs&&... invokable)
      : invokable_(std::forward<FArgs>(invokable)...) {}

  void Run() override {
    Invoke(std::index_sequence_for<FArgs...>{});
    delete this;
  }

 private:
  template <typename T, T... Is>
  void Invoke(std::integer_sequence<T, Is...> ints) {
    std::invoke(std::get<Is>(invokable_)...);
  }

  std::tuple<FArgs...> invokable_;
};

}  // namespace support

template <typename... FArgs>
void Execute(IExecutor& where, FArgs&&... invokable) {
  where.Execute(new support::SimpleTask(std::forward<FArgs>(invokable)...));
}

}  // namespace fio::executor
