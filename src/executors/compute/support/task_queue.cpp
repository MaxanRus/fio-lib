#include <executors/compute/support/task_queue.hpp>
#include <mutex>

namespace fio::executor::compute::support {

void TaskQueue::Push(ITask* task) {
  std::lock_guard guard(guard_);
  queue_.Push(task);
  push_task_or_stop_.notify_one();
}

ITask* TaskQueue::Pop() {
  std::unique_lock guard(guard_);

  if (is_stoped_) {
    return nullptr;
  }

  while (queue_.Empty()) {
    push_task_or_stop_.wait(guard);

    if (is_stoped_) {
      return nullptr;
    }
  }

  return queue_.Pop();
}

bool TaskQueue::IsStoped() const { return is_stoped_; }

void TaskQueue::Stop() {
  std::lock_guard guard(guard_);
  is_stoped_ = true;
  push_task_or_stop_.notify_all();
}

bool TaskQueue::Empty() const {
  std::lock_guard guard(guard_);
  return queue_.Empty();
}

}  // namespace fio::executors::compute::support
