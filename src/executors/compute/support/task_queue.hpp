#pragma once
#include <condition_variable>
#include <executors/task.hpp>
#include <mutex>
#include <support/intrusive/intrusive_forward_list.hpp>

namespace fio::executor::compute::support {

class TaskQueue {
 public:
  void Push(ITask*);
  ITask* Pop();
  bool Empty() const;

  bool IsStoped() const;
  void Stop();

 private:
  ::support::IntrusiveForwardList<ITask> queue_;
  bool is_stoped_{false};
  mutable std::mutex guard_;
  std::condition_variable push_task_or_stop_;
};

}  // namespace fio::executors::compute::support
