#pragma once
#include <executors/compute/support/task_queue.hpp>
#include <executors/executor.hpp>
#include <support/sync/wait_group.hpp>
#include <thread>
#include <vector>

namespace fio::executor::compute {

class ThreadPool : public IExecutor {
 public:
  ThreadPool(size_t workers);

  void Execute(ITask*);
  void WaitIdle();
  void Stop();

  ~ThreadPool() = default;

 private:
  void Work();
  void Join();

  std::vector<std::thread> threads_;
  support::TaskQueue queue_;
  ::support::sync::WaitGroup count_tasks_;
};

}  // namespace fio::executor::compute

