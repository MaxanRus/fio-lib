#include <executors/compute/thread_pool.hpp>

namespace fio::executor::compute {

ThreadPool::ThreadPool(size_t workers) {
  for (size_t i = 0; i < workers; ++i) {
    threads_.emplace_back(&ThreadPool::Work, this);
  }
}

void ThreadPool::Execute(ITask* task) {
  count_tasks_.Add();
  queue_.Push(task);
}

void ThreadPool::WaitIdle() { count_tasks_.Wait(); }

void ThreadPool::Stop() {
  queue_.Stop();
  Join();
}

void ThreadPool::Work() {
  while (true) {
    if (ITask* task = queue_.Pop()) {
      task->Run();
      count_tasks_.Done();
    } else {
      return;
    }
  }
}

void ThreadPool::Join() {
  for (size_t i = 0; i < threads_.size(); ++i) {
    threads_[i].join();
  }
}

}  // namespace fio::executor::compute
