#pragma once
#include <executors/task.hpp>

namespace fio::executor {

class IExecutor {
 public:
  virtual void Execute(ITask*) = 0;
  virtual ~IExecutor() = default;
};

}  // namespace fio::executor

