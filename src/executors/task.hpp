#pragma once
#include <support/intrusive/intrusive_forward_list.hpp>

namespace fio::executor {

struct ITask : public support::IntrusiveForwardListNode<ITask> {
  virtual void Run() = 0;
  virtual ~ITask() {}
};

}  // namespace fio::executor

