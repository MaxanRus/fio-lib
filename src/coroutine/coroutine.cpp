#include <coroutine/coroutine.hpp>
#include <coroutine/coroutine_impl.hpp>

namespace fio::coroutine {

thread_local Coroutine* current = nullptr;

void Coroutine::Resume() {
  Coroutine* save = std::exchange(current, this);
  impl_->Resume();
  current = save;
}

void Coroutine::Suspend() { current->impl_->Suspend(); }

}  // namespace fio::coroutine
