#pragma once
#include <coroutine/coroutine_impl.hpp>

namespace fio::coroutine {

class Coroutine {
 public:
  template <typename F, typename... Args>
  Coroutine(F&& routine, Args... args)
      : stack_(context::Stack::AllocatePages(1)),
        impl_(detail::MakeCoroutine<F>(std::move(routine), stack_,
                                       std::forward<Args>(args)...)) {}

  void Resume();
  static void Suspend();

 private:
  context::Stack stack_;
  detail::CoroutineImpl* impl_;
};

}  // namespace fio::coroutine
