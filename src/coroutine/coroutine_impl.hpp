#pragma once
#include <context.hpp>
#include <functional>

namespace fio::coroutine::detail {

template <typename F, typename... Args>
class CoroutineFunction;

class CoroutineImpl {
 public:
  CoroutineImpl(CoroutineImpl&&) = default;

  void Resume() { previous_context_.SwitchTo(context_); }
  void Suspend() { context_.SwitchTo(previous_context_); }
  bool IsCompleted() const { return is_completed_; }

  virtual ~CoroutineImpl() {}

 protected:
  CoroutineImpl(context::StackView stack) : stack_(std::move(stack)) {}

  context::Context previous_context_;
  context::Context context_;
  context::StackView stack_;
  bool is_completed_ = false;
};

template <typename F, typename... Args>
class CoroutineFunction : public CoroutineImpl, public context::ITrampoline {
 public:
  CoroutineFunction(context::StackView stack, F&& routine, Args... args)
      : CoroutineImpl(stack),
        routine_(std::move(routine)),
        args_(std::forward<Args>(args)...) {
    context_.Setup(stack, *this);
  }

  void Run() override {
    try {
      InvokeRoutine(std::index_sequence_for<Args...>{});
    } catch (...) {
    }
    CoroutineImpl::is_completed_ = true;

    context::Context::ExitTo(previous_context_);
  }

  ~CoroutineFunction() {}

 private:
  template <typename T, T... Is>
  void InvokeRoutine(std::integer_sequence<T, Is...> ints) {
    std::invoke(routine_, std::move(std::get<Is>(args_))...);
  }

  F routine_;
  std::tuple<Args...> args_;
};

template <typename F, typename... Args>
inline CoroutineImpl* MakeCoroutine(F&& routine, context::StackView stack,
                                    Args... args) {
  return new CoroutineFunction<F, Args...>(stack, std::move(routine),
                                           std::forward<Args>(args)...);
}

};  // namespace fio::coroutine::detail
