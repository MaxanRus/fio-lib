#pragma once
#include <cstddef>
#include <span>
#include <string>

namespace fio::fiber::io {

class Socket {
 public:
  Socket();
  Socket(const Socket&) = delete;
  Socket(Socket&&) = default;

  void Bind(size_t port);
  void Listen(size_t count);
  Socket Accept();
  void Connect(const std::string& ip, int port);

  size_t Read(std::span<char> buffer);
  void Write(const std::string&);
  void Write(std::span<char> buffer);
  void Close();

 private:
  Socket(int fd);

  struct sockaddr_in& GetSockAddr();

 private:
  static const size_t sizeof_sockaddr = 16;
  static const size_t alignof_sockaddr = 4;

  alignas(alignof_sockaddr) std::byte t_buff[sizeof_sockaddr];

  int fd_;
};

}  // namespace fio::fiber::io
