#include <fcntl.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <iostream>

#include <fiber/api.hpp>
#include <fiber/io/socket.hpp>

namespace fio::fiber::io {

Socket::Socket() {
  fd_ = socket(AF_INET, SOCK_STREAM, 0);
  fcntl(fd_, F_SETFL, O_NONBLOCK);
}

Socket::Socket(int fd) : fd_(fd) { fcntl(fd_, F_SETFL, O_NONBLOCK); }

void Socket::Bind(size_t port) {
  int val = 1;
  setsockopt(fd_, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));

  auto& address = GetSockAddr();

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(port);

  int res = bind(fd_, (struct sockaddr*)&address, sizeof(address));
}

void Socket::Listen(size_t count) { listen(fd_, count); }

Socket Socket::Accept() {
  auto& address = GetSockAddr();
  int addrlen = sizeof(address);

  int res = -1;
  while (res < 0) {
    res = accept(fd_, (struct sockaddr*)&address, (socklen_t*)&addrlen);
    if (res < 0) {
      Yield();
    }
  }

  return Socket{res};
}

void Socket::Connect(const std::string& ip, int port) {
  auto& address = GetSockAddr();

  address.sin_family = AF_INET;
  address.sin_port = htons(port);
  address.sin_addr.s_addr = inet_addr(ip.c_str());

  int res = connect(fd_, (struct sockaddr*)&address, sizeof(address));
}

sockaddr_in& Socket::GetSockAddr() {
  return *reinterpret_cast<sockaddr_in*>(t_buff);
}

size_t Socket::Read(std::span<char> buffer) {
  int nread = -1;
  while (nread <= 0) {
    nread = read(fd_, buffer.data(), buffer.size());
    if (nread <= 0) {
      Yield();
    }
  }

  return nread;
}

void Socket::Write(const std::string& text) {
  size_t sent = 0;

  while (sent < text.length()) {
    int result = write(fd_, text.data() + sent, text.size() - sent);
    if (result <= 0) {
      fio::fiber::Yield();
      continue;
    } else {
      sent += result;
    }
  }
}

void Socket::Write(std::span<char> text) {
  size_t sent = 0;

  while (sent < text.size()) {
    int result = write(fd_, text.data() + sent, text.size() - sent);
    if (result <= 0) {
      fio::fiber::Yield();
      continue;
    } else {
      sent += result;
    }
  }
}

void Socket::Close() {
  int sut = shutdown(fd_, SHUT_RDWR);
  int res = close(fd_);
}

}  // namespace fio::fiber::io
