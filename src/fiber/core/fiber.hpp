#pragma once
#include <coroutine/coroutine_impl.hpp>
#include <executors/executor.hpp>
#include <fiber/api.hpp>

namespace fio::fiber {

class Fiber {
  friend void detail::MakeFiber(executor::IExecutor&, context::Stack,
                                coroutine::detail::CoroutineImpl*);
  friend void detail::MakeFiber(context::Stack,
                                coroutine::detail::CoroutineImpl*);

 public:
  static Fiber* Self();

  void Step();
  void Resume();
  void Dispatch();

  void Schedule();

  void Suspend(IAwaiter& awaiter);

  executor::IExecutor& GetExecutor() { return executor_; }

 private:
  Fiber(executor::IExecutor& executor, context::Stack stack,
        coroutine::detail::CoroutineImpl* coroutine)
      : executor_(executor),
        stack_(std::move(stack)),
        coroutine_(std::move(coroutine)) {}

  executor::IExecutor& executor_;
  context::Stack stack_;
  coroutine::detail::CoroutineImpl* coroutine_;
  IAwaiter* awaiter_ = nullptr;
};

}  // namespace fio::fiber
