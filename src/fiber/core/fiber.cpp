#include <executors/execute.hpp>
#include <fiber/core/fiber.hpp>

using namespace fio::coroutine;
using fio::coroutine::detail::CoroutineImpl;
using namespace fio::executor;

namespace fio::fiber {

thread_local Fiber* current_fiber = nullptr;

namespace detail {
void MakeFiber(IExecutor& executor, context::Stack stack,
               CoroutineImpl* coroutine) {
  auto new_fiber = new Fiber(executor, std::move(stack), coroutine);
  new_fiber->Schedule();
}

void MakeFiber(context::Stack stack, CoroutineImpl* coroutine) {
  auto new_fiber =
      new Fiber(Fiber::Self()->GetExecutor(), std::move(stack), coroutine);
  new_fiber->Schedule();
}
}  // namespace detail

Fiber* Fiber::Self() { return current_fiber; }

void Fiber::Step() {
  Resume();
  Dispatch();
}

void Fiber::Resume() {
  auto previous_fiber = std::exchange(current_fiber, this);
  coroutine_->Resume();
  current_fiber = previous_fiber;
}

void Fiber::Dispatch() {
  if (!coroutine_->IsCompleted()) {
    awaiter_->OnDispatch(FiberHandle{this});
  }
}

void Fiber::Schedule() { Execute(executor_, &Fiber::Step, this); }

void Fiber::Suspend(IAwaiter& awaiter) {
  awaiter_ = &awaiter;
  coroutine_->Suspend();
}

struct YieldAwaiter : IAwaiter {
  void OnDispatch(FiberHandle fh) override { fh.Schedule(); }
};

void Yield() {
  YieldAwaiter current;
  Fiber::Self()->Suspend(current);
}

void Suspend(IAwaiter& awaiter) { Fiber::Self()->Suspend(awaiter); }

}  // namespace fio::fiber
