#pragma once

namespace fio::fiber {

class Fiber;

class FiberHandle {
  friend Fiber;
 public:
  void Schedule();

 private:
  FiberHandle(Fiber* handle) : handle_(handle) {}
  Fiber* Release();

 private:
  Fiber* handle_ = nullptr;
};

}  // namespace fio::fiber
