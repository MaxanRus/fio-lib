#include <fiber/core/fiber_handle.hpp>
#include <fiber/core/fiber.hpp>

namespace fio::fiber {

void FiberHandle::Schedule() {
  Release()->Schedule();
}

Fiber* FiberHandle::Release() {
  return std::exchange(handle_, nullptr);
}

}  // namespace fio::fiber
