#pragma once
#include <coroutine/coroutine_impl.hpp>
#include <executors/executor.hpp>
#include <type_traits>
#include <fiber/core/fiber_handle.hpp>

namespace fio::fiber {
namespace detail {
void MakeFiber(executor::IExecutor& executor, context::Stack, coroutine::detail::CoroutineImpl* coroutine);
void MakeFiber(context::Stack, coroutine::detail::CoroutineImpl* coroutine);
}  // namespace detail

template <typename F, typename... Args>
void Go(executor::IExecutor& executor, F f, Args... args) {
  auto stack = context::Stack::AllocatePages(4);
  auto coroutine =
      coroutine::detail::MakeCoroutine(std::move(f), stack, std::move(args)...);
  detail::MakeFiber(executor, std::move(stack), coroutine);
}

template <typename F, typename... Args,
          std::enable_if_t<!std::is_base_of_v<executor::IExecutor, F>, bool> = true>
void Go(F f, Args... args) {
  auto stack = context::Stack::AllocatePages(4);
  auto coroutine =
      coroutine::detail::MakeCoroutine(std::move(f), stack, std::move(args)...);
  detail::MakeFiber(std::move(stack), coroutine);
}

struct IAwaiter {
  virtual void OnDispatch(FiberHandle) = 0;
  virtual ~IAwaiter() {}
};

void Yield();
void Suspend(IAwaiter& awaiter);
}  // namespace fio
