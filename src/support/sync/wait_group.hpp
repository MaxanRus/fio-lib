#pragma once
#include <atomic>

namespace support::sync {
class WaitGroup {
 public:
  void Add(size_t count = 1) {
    if (counter_.fetch_add(1) == 0) {
      notifier_.store(false);
    }
  }

  void Done() {
    if (counter_.fetch_sub(1) == 1) {
      notifier_.store(true);
      notifier_.notify_one();
    }
  }

  void Wait() const {
    while (counter_.load() != 0) {
      notifier_.wait(false);
    }
  }

 private:
  std::atomic<size_t> counter_{0};
  std::atomic<bool> notifier_{true};
};
}  // namespace support::sync

