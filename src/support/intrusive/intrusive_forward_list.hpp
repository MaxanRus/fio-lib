#pragma once

namespace support {
template <typename T>
class IntrusiveForwardList;

template <typename T>
class IntrusiveForwardListNode {
  friend class IntrusiveForwardList<T>;
  IntrusiveForwardListNode<T>* next_ = nullptr;
};

template <typename T>
class IntrusiveForwardList {
 public:
  void Push(IntrusiveForwardListNode<T>* new_node) {
    if (head_ == nullptr) {
      tail_ = head_ = new_node;
    } else {
      tail_->next_ = new_node;
      tail_ = new_node;
    }
  }

  T* Pop() {
    T* result = static_cast<T*>(head_);
    head_ = head_->next_;
    return result;
  }

  bool Empty() const { return head_ == nullptr; }

 private:
  IntrusiveForwardListNode<T>* tail_ = nullptr;
  IntrusiveForwardListNode<T>* head_ = nullptr;
};
}  // namespace support

